#!/bin/ksh

echo 'Setting up stream.vidd.ai'
msg_prefix='...'

# check if already have ffmpeg
# usually /usr/local/bin/ffmpeg
is_ffmpeg=`which ffmpeg`
if [ -z $is_ffmpeg ]
then
	# check if already have Homebrew
	# usually in /usr/local/bin/brew

	is_brew=`which brew`
	if [ -z $is_brew ]
	then
		echo "$msg_prefix installing Homebrew"
		/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	else
		echo "$msg_prefix located existing Homebrew installation"
	fi
	echo "$msg_prefix installing ffmpeg"
	brew install ffmpeg
else
	echo "$msg_prefix located existing ffmpeg installation"
fi

# set up for stream.vidd.ai, configure Apache
cd
user=`whoami`
home_dir=`pwd`
echo "$msg_prefix user:$user, home_dir:$home_dir"

if [ ! -d Sites ]
then
    mkdir -p Sites
fi
cd Sites

# clone stream.vidd.ai
if [ ! -d stream.vidd.ai ]
then
	echo "$msg_prefix cloning stream.vidd.ai from bitbucket into $home_dir"
	git clone https://authentiq_bb@bitbucket.org/viddai/stream.vidd.ai.git
	# cd to streaming dir
	cd stream.vidd.ai
else
	# cd to streaming dir
	echo "$msg_prefix stream.vidd.ai already exists, doing a git pull"
	cd stream.vidd.ai
	git pull
fi

# if httpd.conf already exists, overwrite local copy
if [ -f /etc/apache2/httpd.conf ]
then
	echo "$msg_prefix copying httpd.conf"
	cp /etc/apache2/httpd.conf stream.httpd.conf
fi

echo "$msg_prefix configuring Apache"
echo "$msg_prefix Please enter your admin(sudo) password when prompted.."

# back up httpd.conf
if [ ! -f /etc/apache2/httpd.conf.org ]
then
	echo "$msg_prefix backing up httpd.conf"
	sudo cp /etc/apache2/httpd.conf /etc/apache2/httpd.conf.org
fi

# back up httpd-userdir.conf
if [ ! -f /etc/apache2/extra/httpd-userdir.conf.org ]
then
	echo "$msg_prefix backing up httpd-userdir.conf"
	sudo cp /etc/apache2/extra/httpd-userdir.conf /etc/apache2/extra/httpd-userdir.conf.org
fi

# back up USERNAME.conf
ufn='/etc/apache2/users/'$user'.conf'
if [ ! -f $ufn.org ]
then
	echo "$msg_prefix backing up $ufn"
	sudo cp $ufn $ufn.org
fi

# set some vars
echo "$msg_prefix enabling authn_core, authz_host, user_dir modules for Apache"
echo "$msg_prefix enabling php5_module for Apache"
sed -e "s:#\(LoadModule authn_core_module\):\1:" \
-e "s:#\(LoadModule authz_host_module\):\1:" \
-e "s:#\(LoadModule userdir_module\):\1:" \
-e "s:#\(Include /private/etc/apache2/extra/httpd-userdir\):\1:" \
-e "s:#\(LoadModule php5_module\):\1:" \
< stream.httpd.conf > stream.httpd.conf.new

# in http-userdir.conf
sed -e "s:#\(Include /private/etc/apache2/users/\*.conf\):\1:" \
< /etc/apache2/extra/httpd-userdir.conf > stream.httpd-userdir.conf.new

sudo cp stream.httpd.conf.new /etc/apache2/httpd.conf
sudo cp stream.httpd-userdir.conf.new /etc/apache2/extra/httpd-userdir.conf
sudo cp stream.conf $ufn

echo "Restarting apache with new config"
sudo apachectl stop
sudo apachectl start

# create _q.log
echo "$msg_prefix creating _q.log"
> _q.log
chmod 666 _q.log

# create debug.txt
echo "$msg_prefix creating debug.txt"
> debug.txt
chmod 666 debug.txt

# create input_files.json
echo "$msg_prefix creating input_files.json"
> input_files.json
chmod 666 input_files.json

# create input_files_20gb.json
echo "$msg_prefix creating input_files_20gb.json"
> input_files_20gb.json
chmod 666 input_files_20gb.json

# open browser
url='http://localhost/~'$user'/stream.vidd.ai/resizeVideo.php#'
open $url
echo 'stream.vidd.ai setup complete, use your browser to stream your footage files to our server'

