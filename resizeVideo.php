<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
        <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Vidd.ai maps</title>
        <meta name="author" content="Vidd.ai" />

        <link href="https://fonts.googleapis.com/css?family=Merriweather:400,700" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">


        <!-- Animate.css -->
        <link rel="stylesheet" href="animate.css">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="bootstrap.css">
        <!-- Style -->
        <link rel="stylesheet" href="style.css">
        <!-- Modernizr JS -->        <script src="modernizr-2.6.2.min.js"></script>        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>

        <!-- jQuery -->
        <script src="jquery.min.js"></script>

	<style>
	.status {
  	padding: 16px;
  	margin-top: 30px;
  	height: 410px; /* Used in this example to enable scrolling */
	}
	</style>
<body>
        <section id="fh5co-home" data-section="home" style="background-image: url(images/full_image_1.jpg);" data-stellar-background-ratio="0.5">

                <div class="gradient"></div>
                <div class="container">
                        <div class="text-wrap">
                                <div class="text-inner">
                                        <div class="row">
                                                <div class="col-md-8 col-md-offset-2 text-center">
                                                        <h1 class="to-animate">Welcome to Vidd.ai's streaming tool</h1>
                                                        <h2 class="to-animate">Vidd.ai generates a summary of what's inside your raw footage<br/>so you won't have to rummage to find segments to grab and edit.<h2>
                                                        <h2 class="to-animate">Use this tool to send footage files<br/>to our server for processing, upon which you will get your segment map.</h2>
                                                        <p>
                                                        Here's an
                                                        <a href="https://www.vidd.ai/job/mapview?appUserId=5859e410-6c18-4e5a-be5b-06e2fdb2b57d&jobId=1da8a3ba-7405-45bc-9db3-7d6ac84053d7" id="btnExample" target="_blank" class="to-animate">example</a> of a segment map<br/>
                                                        (&copy;casey_willax, <a href="https://www.youtube.com/watch?v=kHS_Wq8D-yM" target="_blank">original</a> on YouTube)</p>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>

        <section id="fh5co-summary" data-section="explore">
                <div class="container summary">
                        <div class="row">
                                <div class="col-md-12 section-heading text-center">
                                        <h2 class="to-animate">To send your footage files to our server<br/>follow 4 simple steps.</h2>
					<br/>
					1. Make sure your email address is filled in
					<p>(We need to send you notification when your processing has completed)</p>
					<br/>
					<span id='restartHere'></span>
					2. Fill in the "Absolute File Path" for the folder where your chosen footage files are located
					<p><a target="_blank" href="
http://osxdaily.com/2009/11/23/copy-a-files-path-to-the-terminal-by-dragging-and-dropping/
">How to copy-paste the correct file path on Macs</a></p><br/>
					3. [optional] Choose a batch of footage files you want processed<p>If you don't choose your files, the first batch of files up to 20 gb will automatically be chosen for you.</p><br/>
					4.  Click the SUBMIT button
					<p>(You'll receive an email notification from our server after your map has been generated)</p><br/>
        				<form id="main-form2" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) . '#restartHere';?>">

                			<input type="hidden" id="vids_to_process2" name="vids_to_process" value="_defaultx">
                			<input type="hidden" id="vids_file_path2" name="vids_file_path" value="_defaulty">
                			<input type="hidden" id="vids_submit" name="vids_submit" value="_defaultz">


                                	</form>
                                        <div class="row">
        					<div class="col-lg-8 col-lg-offset-2 to-animate">

                                                        <div id="emailDiv" class="form-group">
 								<div class="input-group">
    								<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
    								<input id="EmailAddress2" type="text" class="form-control" name="email" placeholder="Your email">
     								<span id="EmailAddress2Ok" class="glyphicon glyphicon-ok form-control-feedback"></span>
  								</div>
 								<div class="input-group">
    								<span class="input-group-addon"><i class="glyphicon glyphicon-folder-open"></i></span>
    								<input id="uploadFilePath2" type="text" class="form-control" name="file path" placeholder="Path of folder where your footage files are, example: /Volumes/GOPRO/Cancun Diving/20-Dec">
     								<span id="uploadFilePath2Ok" class="glyphicon glyphicon-ok form-control-feedback"></span>
  								</div>
    								<select id="listInputFiles" multiple class="form-control" style="display:none">

            						</div>
							<input id="btnUpload2" class="btn btn-primary" placeholder="Submit" style="display:none">
        					</div>

                                        </div>
                                        <div id="hasEmailDiv" class="row" style="display:none;"></div>

                              </div>
                        </div>
                        <div class="row">
                                <div class="col-md-8 col-md-offset-2 to-animate">
                                        <div style="margin-top:20px">
                                        <p id="fileStatusElement"></p>
                                        </div>
                                </div>
                        </div>
              	<hr />
                <footer>
		<div class="btn-group">
			<label id="helpBtn" class="btn btn-primary btn-xs">
                        Get help
			</label>
		</div>
		<div id="helpDiv" class="row">
		</div>
                </footer>

             </div>
	</section>

	<?php
		$debug = 0;
		// for debugging
   		$cookie_name = 'u';
		if ($debug) {
   		if(!isset($_COOKIE[$cookie_name])) {
       			echo "Cookie named '" . $cookie_name . "' is not set!";
   		} else {
       			echo "Cookie '" . $cookie_name . "' is set:'" . $_COOKIE[$cookie_name] . "'<br/>";
   		}
		}
	?>

	<?php
	
	define("DEBUG_FN", './debug.txt');
	define("SCRATCH_PATH", '/tmp/');

	function logDebug($msg) {
		$debug_fn = DEBUG_FN;
		$msg .= "\n";
		file_put_contents ($debug_fn, $msg, FILE_APPEND);
	}

	function writeQMgr() {
		$Qmgr = '/tmp/_q.sh';
		$Qmgr_run = '/tmp/___q.sh';
		$Qlog = '_q.log';
                $Qmgr_cmd = <<<QMGR
#!/bin/ksh

# I am running!
> /tmp/___q.sh

cycles=0
max_cycles=10

# file to store status
q_log='_q.log'

# success, errors
succ_fn=''
err_fn=''
delim=''
succ_cnt=0
err_cnt=0

while [ True ]
do
        # start with empty log
        echo Qmgr: cycle[\$cycles]: checking for jobs in the queue'<br/>' > \$q_log

        # grab oldest 3 files
        res=`ls -t /tmp/*segmentSend.php  | tail -n3`
        if [ ! -z \${res} ]
        then

        # parse to get filename
        for fn in `echo \$res`
        do
                echo fn:\$fn
                basefn=`basename \$fn`
                basefn=`echo \$basefn | sed -e 's/.segmentSend.php//g'`
		# basefn without user
                basefn=`echo \$basefn | cut -d'.' -f2,3`
                if [ ! -z  "\${fn}" ]
                then
                        log=\$fn'.log'
                        # if log exists, file was already kicked off
                        if [ -f  "\${log}" ]
                        then
                                res2=`tail -n1 \$log | grep "^<pre>status:"`

                                # status check, php execution complete
                                if [ ! -z  "\${res2}" ]
                                then
                                        succ=`echo \$res2 | grep "success"`
                                        if [ ! -z "\${succ}" ]
                                        then
                                                ((succ_cnt=\$succ_cnt+1))
                                                if [ ! -z "\${succ_fn}" ]
                                                then
                                                        delim=','
                                                fi
                                                succ_fn=\$succ_fn\$delim\$basefn
                                        else
                                                ((err_cnt=\$err_cnt+1))
                                                if [ ! -z "\${err_fn}" ]
                                                then
                                                        delim=','
                                                fi
                                                err_fn=\$err_fn\$delim\$basefn
                                        fi
                                        rm -f \$log
                                        rm -f \$fn
                                        echo \$basefn complete:'<br/>' >> \$q_log
                                        echo \$res2'<br/>' >> \$q_log
                                else
                                        echo \$basefn in progress '<br/>' >> \$q_log
                                fi
                        else
                                php \$fn 2>&1 > \$log &
                                echo kicked off \$basefn > \$q_log
                        fi
                fi
        done
        fi
        sleep 3
        if [ -z  "\${res}" ]
        then
                if [ \${cycles} -gt \$max_cycles ]
                then
                        break
                fi
                ((cycles=\$cycles+1))
                echo Qmgr: waiting for new jobs, cycle[\$cycles]'<br/>' > \$q_log
                echo Last batch successfully sent to server: \$succ_cnt : \$succ_fn'<br/>' >> \$q_log
                echo Unsuccessful: \$err_cnt : \$err_fn'<br/>' >> \$q_log
        else
                echo Qmgr: cycle[\$cycles]: jobs in the queue > \$q_log
        fi
done
echo Last batch successfully sent to server: \$succ_cnt : \$succ_fn'<br/>' >> \$q_log
echo Unsuccessful: \$err_cnt : \$err_fn'<br/>' >> \$q_log
echo Qmgr: no jobs in the last \$cycles cycles, resting..'<br/>' >> \$q_log

# I am exiting!
rm -f /tmp/___q.sh
exit
QMGR;
                if (file_put_contents($Qmgr, $Qmgr_cmd)) {
                        //echo "wrote $Qmgr";
                        logDebug ("wrote $Qmgr");
			return(1);
                } else {
			return(-1);
                }


	}

	function makeQMgr() {
		$Qmgr = '/tmp/_q.sh';
		$Qmgr_run = '/tmp/___q.sh';

		// $Qlog needs to be in www-server-accessible path, defaults to './'
		$Qlog = '_q.log';
		// create $Qlog with write privileges
		$f = fopen($Qlog, "w") or die ("unable to write $Qlog") ;
		fclose($f);
		if (writeQMgr() < 1) {
			//echo "cannot write $Qmgr";
			logDebug ("cannot write $Qmgr");
		}
		if ((file_exists($Qmgr)) && (! file_exists($Qmgr_run))) {
			logDebug ("kicking off $Qmgr");
			// kick off Qmgr
                	$cmd = "ksh $Qmgr 2>/dev/null >/dev/null &";
			$output = shell_exec($cmd);
		}
	}

	function makesegmentSend($input_path, $fn) {

                $scratch_path = SCRATCH_PATH;
		$debug_fn = DEBUG_FN;

                // get base fn and ext
                list ($tfn, $text) = explode('.', $fn);
                $tfn = preg_replace('/\s+/', '', $tfn);

                // remove all non-alphanumeric characters from base_fn fpr $pl_fn
                $pl_fn = preg_replace('/[^A-Za-z0-9]/', "", $tfn) . '.' . $text;
                if ($debug) {echo ' pl_fn:'.$pl_fn.' ';}


                // escape all chars in filename special to the shell for $ffmpeg_fn, including \s
		$regex = '/([\s\/\\\'\"\*\?\[\-\]\{\}\~\$\!\&\;\(\)\<\>\|\#\@])/';
		$replace = '\\\$1';
		$ffmpeg_fn = preg_replace($regex ,$replace ,$fn);

                $ffmpeg_path = "/usr/local/bin/ffmpeg";
                $ffprobe_path = "/usr/local/bin/ffprobe";

		$user = md5($_COOKIE['u']);
		$user_fn = $scratch_path.$user . '.' . $pl_fn . '.u-json';

		$json_fn = $scratch_path.$user . '.' . $pl_fn . '.json';

                $std_err = " 2>&1";
                $std_err = "";

                echo '<script>document.getElementById("fileStatusElement").innerHTML="examining:' . $fn . '";</script>';
                // aws instance
                $server_cmd = "http://stream.vidd.ai/receiveTS.php";

                $ok_status = '/remote server ok/';

                $playlist = $scratch_path . $user . '.' . $pl_fn . '.playlist.m3u8';
                //$pl_cmd = "grep ^/tmp " . $playlist;
                $pl_cmd = "grep ^$scratch_path " . $playlist;

		$user_info = array('user' => $user, 'contact' => $_COOKIE['u'], 'input_path' => $input_path);
		file_put_contents($user_fn, json_encode($user_info));

		logDebug ("input_path_ffmpeg_fn:$input_path$ffmpeg_fn:");
		logDebug ("pl_fn:$pl_fn:");

                $ffprobe_cmd = <<<FFPROBE
$ffprobe_path -v quiet -print_format json -show_streams $input_path$ffmpeg_fn \
> $json_fn
FFPROBE;

                $ffmpeg_cmd = <<<FFMPEG
$ffmpeg_path -i $input_path$ffmpeg_fn -filter_complex '[0:v]yadif'\
        -s 426x240  -an -vcodec h264  \
 -vsync 0 \
 -copyts \
 -movflags frag_keyframe+empty_moov \
 -hls_flags delete_segments+append_list \
 -f segment \
 -f segment \
 -segment_list_flags live \
 -segment_time 1 \
 -segment_list_size 0 \
 -segment_format mpegts \
 -segment_list $playlist\
 -segment_list_type m3u8 \
 -segment_list_entry_prefix $scratch_path \
 $scratch_path$user.$pl_fn.%d.ts
FFMPEG;

                // construct $ofn.segmentSend.php
                $php_str= <<<EOF
<?php
\$__server_="$server_cmd";

// user info
\$cmd='curl --form "image_data=@' . '$user_fn' . '" ' . \$__server_;
\$output = shell_exec(\$cmd);
// clean up
\$cmd='rm -f ' . '$user_fn';
\$output = shell_exec(\$cmd);

// ffprobe
\$cmd = "$ffprobe_cmd";
\$output = shell_exec(\$cmd);
\$cmd='curl --form "image_data=@' . '$json_fn' . '" ' . \$__server_;
\$output = shell_exec(\$cmd);

// clean up
\$cmd='rm -f ' . '$json_fn';
\$output = shell_exec(\$cmd);

// ffmpeg .ts
\$cmd = "$ffmpeg_cmd";
\$output = shell_exec(\$cmd);

// send .ts files
if (! file_exists('$playlist')) {
        die('<pre>status: error m3u8 was not created</pre>');
}
\$cmd = "$pl_cmd";
\$output = shell_exec(\$cmd);
file_put_contents('$debug_fn',"ts_files:\$output\\n", FILE_APPEND);

\$ts_files = explode("\n",\$output);

\$num_files = sizeof(\$ts_files);
\$ok_status = "$ok_status";
\$last_file_sent = 0;

file_put_contents('$debug_fn',"num_files:\$num_files\\n", FILE_APPEND);


foreach (\$ts_files as \$ts_file) {
        if (empty(\$ts_file)) {
                \$num_files--;
                continue;
        }
        \$cmd='curl --form "image_data=@' . \$ts_file . '" ' . \$__server_;
        \$output = shell_exec(\$cmd);

	file_put_contents('$debug_fn',"curl output:\$output\\n", FILE_APPEND);

        if (!(preg_match(\$ok_status, \$output, \$matches))) {
                break;
        }
        \$num_files--;
        \$last_file_sent++;

        echo "\n<pre>\$output</pre>";

}

// clean up
foreach (\$ts_files as \$ts_file) {
        if (empty(\$ts_file)) {
                continue;
        }
        \$cmd='rm -f ' . \$ts_file;
        \$output = shell_exec(\$cmd);
}
\$cmd='rm -f ' . '$playlist';
\$output = shell_exec(\$cmd);

if (\$num_files != 0) {
        \$output = "status:error streaming $playlist";
} else {
	// make and send EOF to server
	\$eof='$scratch_path$user.$pl_fn.' . \$last_file_sent. '.EOF';
	\$cmd='> ' . "\$eof";
	\$output = shell_exec(\$cmd);
	\$cmd='curl --form "image_data=@' . \$eof . '" ' . \$__server_;
	\$output = shell_exec(\$cmd);
	\$cmd='rm -f ' . "\$eof";
	\$output = shell_exec(\$cmd);

        \$output = 'status:success, ' . sizeof(\$ts_files) . ' ts files sent to remote server for segmenting';
}
echo "\n<pre>\$output</pre>";


?>
EOF;
        	// write to file
        	list ($tfn, $text) = explode('.', $fn);
        	$tfn = preg_replace('/\s+/', '', $tfn);
        	$ssfn = $scratch_path . $user . '.' . preg_replace('/[^A-Za-z0-9]/', '', $tfn) . '.' . $text . '.' . 'segmentSend.php';
        	if ($debug) {echo 'ssfn:'.$ssfn;}
        	if (file_put_contents ($ssfn, $php_str)) {
			logDebug ("created segmentSend file: $ssfn");
			return(1);
        	} else {
			//echo "could not write segmentSend file:$ssfn"; 
			logDebug ("could not write segmentSend file:$ssfn"); 
			return(-1);
		}

	}

	function makeDirList($input_path) {
		// get list of video files in $input_path
                              $regex = "*.{mov,mp4,3gp,avi,mpg,vob,m2ts,mkv,MOV,MP4,3GP,AVI,MPG,VOB,M2TS,MKV}";
                                $s = $input_path . $regex;
                                logDebug("glob input_path:$s:");

                                $input_files = [];
				$input_files_20gb = [];
                                logDebug("empty input_files");
                                $t_input_files = glob($s, GLOB_BRACE);
                                logDebug("t_input_files:".json_encode($t_input_files));
                                $t_fs = 0;
                                foreach ($t_input_files as $filename) {
                                        $fs = filesize($filename);
                                        logDebug("$filename $fs " . date("F d Y H:i:s.",filectime($filename)));
                                        $fr = array("fn" => $filename, "fs" => $fs, "ft" => date("F d Y H:i:s A",filectime($filename)));
                                        if (($t_fs + $fs) > 20000000000) {
					// if file doesn't fit into 20gb limit, keep track and continue
                                        	array_push($input_files, $fr);
                                                continue;
                                        }
                                        //array_push($input_files, $filename);
                                        array_push($input_files, $fr);
                                        array_push($input_files_20gb, $filename);
                                        $t_fs += $fs;

                                }
			$r_input_files = array("input_files" => $input_files);
			$r_input_files_20gb = array("input_files" => $input_files_20gb);

			file_put_contents('input_files.json', json_encode($r_input_files));
			file_put_contents('input_files_20gb.json', json_encode($r_input_files_20gb));
                        logDebug("resulting input_files:".json_encode($r_input_files));
			return ($input_files);
	}

	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (isset ($_POST["vids_to_process"])) {
			//var_dump($_POST["vids_to_process"]);
			//var_dump($_POST["vids_file_path"]);

			$input_path = trim($_POST["vids_file_path"]);
			$t_input_files = trim($_POST["vids_to_process"]);
			//$input_files = explode(",",trim($_POST["vids_to_process"]));

			$_ret = 0;

///
                        // make it easy for user: if $input_path does not end in '/', grab the longest path in string
			logDebug('~~~ new post ~~~');
			logDebug("original input_files:".$_POST["vids_to_process"].":");
                        logDebug("original input_path:$input_path:");
                        $regex = '/[^\/]$/';
/*
                        if ( preg_match ($regex, $input_path)) {
                                $regex = '/(.+\/)(.+$)/';
                                $replace = '$1';
                                $input_path = preg_replace($regex ,$replace ,$input_path);
                                logDebug("new input_path:$input_path:");
                        }
*/
///

			// if input_path doesn't end in a slash, add it on
                        $regex = '/[^\/]$/';
                        if ( preg_match ($regex, $input_path)) {
				$input_path .= '/';
			}

			// if user has not submitted, grab file listing in input_path
			if (isset($_POST["vids_submit"]) && ($_POST["vids_submit"] != 'submit' )) {
				$input_files = makeDirList($input_path);
				return;
			}
			else {
				// $t_input_files is still a string
				if ($t_input_files) {
					logDebug("submit: original t_input_files not empty:".$t_input_files.":");
					$input_files = explode(",",trim($_POST["vids_to_process"]));
				} else {
				// if user did not choose files, grab first files that satisfy 20gb
					logDebug("submit: getting _20gb.json files");
					$string = file_get_contents("input_files_20gb.json");
					$json_a = json_decode($string, true);
					$input_files = $json_a["input_files"];
				}
			}

			// process each file
			foreach ($input_files as &$fn) {
               			if (empty($fn)) {
					continue;
				}
				// 20gb filenames still have path, remove it
				$fn = basename($fn);
				$_ret = makesegmentSend($input_path, $fn);
			} // for
			if ($_ret > 0) {
				makeQMgr();
			}
		} else {
			//echo 'vids_to_process not set';
			logDebug ('vids_to_process not set');
		}


	}

	function test_input($data) {
  		$data = trim($data);
  		$data = stripslashes($data);
  		$data = htmlspecialchars($data);
  		return $data;
	}

   	if (isset($_GET["u"])) {
   		$cookie_value = htmlspecialchars($_GET["u"]); //username
  		if ($debug) {echo "setting cookie:" . $cookie_value . "<br/>";}
		setcookie($cookie_name, $cookie_value, time() + (10 * 365 * 24 * 60 * 60), "/"); // does not expire 
   	}

	?>

 
<script>
  var user;
  var interval_id = -1;
  var vids_to_process = new Array();

</script>

<!-- Cookies -->
<script src="vidd.cookie.js"></script>

<script>
  	var user=getCookie("u");
       	var userSetViaEmail = 0;

        $(document).ready(function () {
                // check if we already have this user
                if (user != "" && isEmail(user)) {
                        $('#EmailAddress').val(user);
                        $('#EmailAddress2').val(user);
                        $('#EmailAddress2').css('background-color', 'gray');
			$('#EmailAddress2Ok').css('color','green');
			$('#uploadFilePath2').focus();
                } else {
                        $('#emailDiv').show();
                        var msg = "Enter your email address (you'll receive notification after processing)";
                        $('#EmailAddress').attr('size',msg.length>50?50:msg.length);
                        $('#EmailAddress').val(msg);
                        $('#EmailAddress2').attr('size',msg.length>50?50:msg.length);
                        $('#EmailAddress2').val(msg);
                        userSetViaEmail = 1;
                }

        });

  function checkDebugLog() {
    var pre1 = '<pre>';
    var pre2 = '</pre>';
    var debug_fn = './debug.txt';
    $.ajax({
        url: debug_fn,
        type: "GET",
        cache: false,
        success: function (data){
                var date = new Date();
                var time = date.toLocaleTimeString();
		if (data) {
			var msg = 'Send below to founder.vidd.ai at gmail';
			msg = msg + "\n\n" + data;
                	$("#helpDiv").fadeTo("slow", 0.4).html(pre1+ msg + pre2);
		}
		else {
			var msg = 'Contact founder.vidd.ai at gmail';
			$("#helpDiv").fadeTo("slow", 0.4).html(pre1+ msg + pre2);
		}
      },
        fail: function (data){
            console.error('failed to get debug.txt:'+ data);
      }

    });

  }


  function checkLogs() {
    var status = 'status:';
    $.ajax({
        url: "_q.log",
        type: "GET",
	cache: false,
      	success: function (data){
            console.log('_q.log:'+ data);
		var date = new Date();
		var time = date.toLocaleTimeString();
		$("#fileStatusElement").fadeTo("slow", 0.4).html(status+ ' ' + time + ' : ' + data);
		var res = data.match(/resting/g);
		if (res) {
            		console.log('stopping interval'+ res);
			clearInterval(interval_id);
		}
      },
      	fail: function (data){
            console.error('failed:'+ data);
      }

    });

  }

  function checkDir() {

    $('#vids_to_process2').val(vids_to_process);
    $('#vids_file_path2').val($('#uploadFilePath2').val());

    console.log('checkDir:after reassigning vals');
    var form=document.querySelector('#main-form2');
    var formData = new FormData(form);

    // Display the key/value pairs
    for (var pair of formData.entries()) {
            console.log('form.entry:' + pair[0]+ ', ' + pair[1]);
    }

    var status = 'Files:';
    var form = $('#main-form2');
    var url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (){
   		$.getJSON('input_files.json', function (data) {
			var input_files = data["input_files"];
			if (input_files.length == 0) {
				$("#fileStatusElement").html("Cannot find any video files in that folder, please check the folder path and try again");
				return;
			}
			$("#fileStatusElement").html("");
			var html = '';
			$.each(data["input_files"], function (key, val) {
				var basename = val.fn.replace(/^.*[\\\/]/, '');
				//html += '<option>' + val.fn + '&nbsp;&nbsp;&nbsp;&nbsp;' + '&nbsp;&nbsp;&nbsp;&nbsp;' + Number(val.fs/1000000000).toFixed(2) + ' gb' + '&nbsp;&nbsp;&nbsp;&nbsp;' + '&nbsp;&nbsp;&nbsp;&nbsp;' + val.ft + '<br/>';
				html += '<option>' + basename + ' ' + '&nbsp;&nbsp;&nbsp;&nbsp;' + '&nbsp;&nbsp;&nbsp;&nbsp;' + Number(val.fs/1000000000).toFixed(2) + ' gb' + '&nbsp;&nbsp;&nbsp;&nbsp;' + '&nbsp;&nbsp;&nbsp;&nbsp;' + val.ft + '<br/>';
				html += '</option>';
			});
			html += '</select>';
                	//$("#showInputFiles").fadeTo("slow", 0.9).html(html);
                	$("#showInputFiles").show();
                	$("#listInputFiles").show().html(html);
                        $('#uploadFilePath2').css('background-color', 'gray');
			$('#uploadFilePath2Ok').css('color','green');
			$("#btnUpload2").fadeIn("slow");
		});

      },
        fail: function (data){
            console.error('failed:'+ data);
      }

    });

  }

  // first kick off  
  if (interval_id == -1) {
 	interval_id = setInterval("checkLogs()", 3000);
  }


  function ProcSegments() {
       if (interval_id == -1) {
 	   interval_id = setInterval("checkLogs()", 3000);
       }

	$('#vids_to_process2').val(vids_to_process);
	$('#vids_file_path2').val($('#uploadFilePath2').val());

	console.log('after reassigning vals');
	var form=document.querySelector('#main-form2');
	var formData = new FormData(form);

	// Display the key/value pairs
	for (var pair of formData.entries()) {
    		console.log('form.entry:' + pair[0]+ ', ' + pair[1]); 
	}
	$('#main-form2').submit();
  }

  function UploadFile(TargetFiles) {
	   vids_to_process = [];
           var fileCount = TargetFiles.length;

            var i = 0;
            TotalSize = 0;

            for (i = 0; i < fileCount; i++) {
                var file = TargetFiles[i];
                vids_to_process.push(file.name);
            }
	    console.log('vids_to_process:',vids_to_process);
            ProcSegments();
  }


  $('select').on('change', function() {
	var v = $('#listInputFiles').val();
	var a = [];
	for (i=0; i< v.length; i++) {
  		var fn = v[i].split(' ')[0];
        	a.push(fn);
	}
	// dedupe using Set()
        vids_to_process = [...new Set(a)];
	console.log('vids_to_process:',vids_to_process);
  });

  $('#uploadFilePath2').on('keyup', function () {
	//alert('show dir');
	checkDir();
  });

  $('#btnUpload2').click(function () {
		// revalidate email
                if (!isEmail($('#EmailAddress2').val())) {
                        document.getElementById("fileStatusElement").innerHTML = "Please enter a valid email address.";
                        // blink 3 times
                        for(i=0;i<3;i++) {
                                $('#fileStatusElement').fadeTo('fast', 0.5).fadeTo('fast', 1.0);
                        }

                        return;
                }
                // set username cookie to email address here
                user = $('#EmailAddress2').val();
                setCookie("u", user, 30);
		$('#EmailAddress2Ok').show();

		// go directly to ProcSegments
		$('#vids_submit').val('submit');
	    	console.log('vids_to_process:',vids_to_process);
  		if (interval_id == -1) {
 			interval_id = setInterval("checkLogs()", 3000);
		}
		ProcSegments(); return;

                if (!($('#uploadFilePath2').val())) {
                        document.getElementById("fileStatusElement").innerHTML = "Please enter the absolute file path for the folder where your footage files are.<br/>For example: '/Volumes/Videos/100GOPRO/'";
                        // blink 3 times
                        for(i=0;i<3;i++) {
                                $('#fileStatusElement').fadeTo('fast', 0.5).fadeTo('fast', 1.0);
                        }

                        return;
                }
                UploadFile($('#uploadFile2')[0].files);
  }
  )

  $('#helpBtn').click(function () {
	checkDebugLog();
  })


  function isEmail(email) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(email);
  }



</script>


        <!-- Bootstrap -->
        <script src="bootstrap.min.js"></script>
        <!-- Waypoints -->
        <script src="jquery.waypoints.min.js"></script>
        <!-- Stellar Parallax -->
        <script src="jquery.stellar.min.js"></script>

        <!-- Main JS (Do not remove) -->
        <script src="main.js"></script>

</body>
</html>
